class LinearCongruentialEngine {
  constructor(seed, a, c, m) {
    seed = Math.round(seed);
    this.a = a || 16807;
    this.c = c || 0;
    this.m = m || 2147483647;

    this.state = seed;
  }

  get() {
    this.state = (this.a * this.state + this.c)  % this.m;
    return this.state;
  }

  min() { return 0; }

  max() { return this.m; }
};

var DefaultRandomEngine = LinearCongruentialEngine;

class UniformRealDistribution {
  constructor(a, b) {
    this.a = a;
    this.b = b;
  }

  get(rnd) {
    return this.constructor.getUniform(this.a, this.b, rnd);
  }

  static getUniform(min, max, rnd) {
    var r =  (rnd.get() - rnd.min()) / (rnd.max() - rnd.min()); // [0, 1)
    return r * (max - min) + min;
  }
};

export { LinearCongruentialEngine, DefaultRandomEngine, UniformRealDistribution };
