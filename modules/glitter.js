/* Copyright (C) 2019 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {vec2, vec3, mat4, quat} from './gl-matrix/index.js';
import * as Random from './random.js';

function directionFromEuler(alpha, beta, gamma) {
  let rot = quat.fromEuler(quat.create(), alpha, beta, gamma);
  let mat = mat4.fromQuat(mat4.create(), rot);
  let zVec = vec3.fromValues(0,0,1);

  return vec3.transformMat4(zVec, zVec, mat);
}

var defaultRandomU = new Random.DefaultRandomEngine(1234);
function randomUniform(min, max) {
  return Random.UniformRealDistribution.getUniform(min, max, defaultRandomU);
}

function hash(x) {
  const p = 11
  const q = 19
  const M = p*q
  x = x % M;
  return x*x % M;
}

function normalizeHash(x) {
  const M = 11*19;
  return x / M;
}

function hash2(x,y) {
  return hash(x + hash(y));
}

function quantize(x, step) {
  return Math.round(x/step)*step;
}
function rand2(x){
   return fract(Math.sin(vec2.dot(x, vec2.fromValues(12.9898,78.233))) * 43758.5453);
}
function rand2Int(x){
   return Math.round(100*(Math.sin(vec2.dot(x, vec2.fromValues(12.9898,78.233))) * 43758.5453));
}


class Particles {
  constructor() {
    this.angularResolution = 360 / 40;
    this.density = 1e-6;
    this.width = 100;
    this.height = 100;
  }

  get(angleX, angleY) {
    angleX = quantize(angleX, this.angularResolution);
    angleY = quantize(angleY, this.angularResolution);

    let coords = [];
    let numParticles = Math.max(1, Math.round(this.width*this.height*this.density* // FIXME stochastic sampling
      2*this.angularResolution*(0.8+0.2*rand2(vec2.fromValues(angleX,angleY)))));
    var rndE = new Random.DefaultRandomEngine(12+Math.abs(rand2Int(vec2.fromValues(angleX,angleY))));
    var rnd = function(max) {
      return Random.UniformRealDistribution.getUniform(0, max, rndE);
    }
    for (let i=0; i<numParticles; ++i) {
      coords[i] = vec2.fromValues(rnd(this.width), rnd(this.height));
    }

    return coords;
  }

  // returns the coordinates of all particles oriented such that they reflect.
  // The angle represents their reflection vector. i.e. the reflection is
  // strongest when it is equal to the supplied angle in the argument (like in
  // Phong shading)
  getAll(angleX, angleY, deltaAngle) {
    console.assert(deltaAngle > this.angularResolution);

    angleX = quantize(angleX, this.angularResolution);
    angleY = quantize(angleY, this.angularResolution);
    let coords = [];
    for (let y=-deltaAngle; y<deltaAngle; y+=this.angularResolution) {
      for (let x=-deltaAngle; x<deltaAngle; x+=this.angularResolution) {
        if (x*x + y*y < deltaAngle*deltaAngle) {
          coords.push({angleX: angleX+x, angleY: angleY+y,
            coords: this.get(angleX+x, angleY+y)});
        }
      }
    }
    return coords;
  }
}

function clamp(value) {
  return Math.max(0, Math.min(value, 1));
}
function noise(value) {
  const subdiv = 1024;
  return (value * (subdiv-1)) % subdiv;
}
function fract(x) {
  return x - Math.floor(x);
}
function noise2(x,y) {
  return fract(Math.sin(x*12.9898+y*78.233) * 43758.5453);
}
function noiseVec3(vec) {
  return noise2(vec[0],vec[1]);
}

// Draws glitter. Sparkels based on the smartphone orientation. Use as overlay
// with e.g. the background-image style or supply an bgImage to the constuctor
// to allow the alpha value to be used as mask.
// Glitter.color can be set to the desired color of the glitter or to
// 'iridescence' for rainbow sparkles
export default class Glitter {
  constructor(canvas, bgImage) {
    if (bgImage) {
      this.hasBgImage = true;
      bgImage.onload = () => {
        const width = bgImage.naturalWidth;
        const height = bgImage.naturalHeight;

        let canvas;
        if (false && OffscreenCanvas) {
          canvas = new OffscreenCanvas(width, height);
        } else {
          canvas = document.createElement('canvas');
          canvas.width = width;
          canvas.height = height;
        }

        let ctx = canvas.getContext('2d');
        ctx.drawImage(bgImage, 0, 0);

        const imageData = ctx.getImageData(0,0, width, height);
        this.bgImageData = imageData;

        this.resize(width, height);
      };
    }

    this.color = 'white';

  this.angles = vec3.fromValues(0,0,0); // in degree
  this.animateAngle = 0; // in radians
  this.radius = 1;
  this.bokeh = 1;
  this.specular = 80;
  this.alphaCutoff = 0.1;
  /* alpha = b^spec
   * b = alpha^-spec
   *  alpha < e
   *  => b < e^-spec
   *  b = orientation dot normal
   *    a dot b = cos theta (if a and b are of unit length)
   *  => theta < acos(e^-spec)
   *
   * angle in degree
   */
  this.maxDeltaAngle = Math.acos(Math.pow(this.alphaCutoff, 1/this.specular))/Math.PI*180;
  this.angleSubdivisions = 10;

    this.canvas = canvas;
    this.ctx = canvas.getContext('2d');

    this.particles = new Particles();
    this.particles.angularResolution = this.maxDeltaAngle*2/this.angleSubdivisions;

    this.resize();
  }
  resize(width, height) {
    if (width && height) {
      this.canvas.width = width;
      this.canvas.height = height;

    }
    this.particles.width = this.canvas.width;
    this.particles.height = this.canvas.height;
  }

  bgPixelAlpha(x, y) {
    if (!this.bgImageData) return null;

    x = Math.round(x);
    y = Math.round(y);

    const img = this.bgImageData;
    if (x < 0 || x > img.width) return null;
    if (y < 0 || y > img.height) return null;

    const i = (img.width * y + x) * 4;
    const d = img.data;

    return d[i+3];
  }

  draw() {
    // sparkle somewhat when stationary
    this.animateAngle = (this.animateAngle + 0.02) % (2*Math.PI);
    const dAngle = 3; // degree
    const dAngleX = dAngle * Math.cos(this.animateAngle);
    const dAngleY = dAngle * Math.sin(this.animateAngle);

    const orientationAngleX = this.angles[1]+dAngleX;
    const orientationAngleY = this.angles[2]+dAngleY;

    // orientation from Euler angles is probably wrong but good enough.
    // Also the first angle is not considered. In principle it should be used
    // to rotate the two other angles.
    this.orientation = directionFromEuler(0, orientationAngleX, orientationAngleY);
    vec3.normalize(this.orientation, this.orientation);

    const ctx = this.ctx;

    ctx.clearRect(0,0,this.canvas.width, this.canvas.height);
    if (this.bgImageData) {
      ctx.putImageData(this.bgImageData, 0, 0);
    }

    if (this.color != 'iridescence') {
      ctx.fillStyle = this.color;
      ctx.shadowColor = this.color;
    }
    ctx.shadowBlur = this.bokeh;

    let w = this.radius+this.bokeh;

    var coords = this.particles.getAll(orientationAngleX, orientationAngleY, this.maxDeltaAngle);
    coords.forEach(cc => {
      const normal = directionFromEuler(0, cc.angleX, cc.angleY);
      vec3.normalize(normal, normal);
      const specBase = clamp(vec3.dot(this.orientation, normal));
      let spec = Math.pow(specBase, this.specular);
      ctx.globalAlpha = spec;
      if (this.color == 'iridescence') {
        ctx.fillStyle = `hsl(${360*spec}, 100%,  ${100*spec}%)`;
        ctx.shadowColor = ctx.fillStyle;
      }

      cc.coords.forEach(c => {
        if (this.hasBgImage) {
          const alpha = this.bgPixelAlpha(c[0],c[1]);
          if (!alpha) return; // null or 0
        }

        ctx.beginPath();
        ctx.arc(c[0],c[1], this.radius, 0, 2*Math.PI);
        ctx.fill();
      });
    });
  }

  viewAngles(alpha, beta, gamma) {
    this.angles[0] = alpha;
    this.angles[1] = beta;
    this.angles[2] = gamma;
  }
  redraw() {
    this.draw();
    window.requestAnimationFrame(() => this.redraw());
  }

  run() {
    window.addEventListener('deviceorientation', (e) => {
      this.viewAngles(e.alpha, e.beta, e.gamma);
    }, true);
    window.requestAnimationFrame(() => this.redraw());

    if (window.DeviceOrientationEvent && window.DeviceOrientationEvent.requestPermission) {
      // FIXME request cached permission without user interaction?
      console.log('request permission');
      const canvas = this.canvas;
      function requestPermission() {
        let result = window.DeviceOrientationEvent.requestPermission();
        canvas.removeEventListener(requestPermission);
      }
      canvas.addEventListener('click', requestPermission());
    }
  }
}
