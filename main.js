/* Copyright (C) 2019 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Glitter from './modules/glitter.js';

//https://html.spec.whatwg.org/multipage/canvas.html#dom-context-2d-fillstyle
function serializeColor(color) {
  const ctx = document.createElement('canvas').getContext('2d');
  ctx.fillStyle = color;
  return ctx.fillStyle;
}

function init() {
  let canvas = document.getElementById('glitter');
  var glitter = new Glitter(canvas);
  window.addEventListener('resize', () => {
    glitter.resize(window.innerWidth, window.innerHeight);
  }, true);
  glitter.resize(window.innerWidth, window.innerHeight);

  glitter.run();

  // allow sparkle on mousemove for development/debugging on a desktop
  window.addEventListener('mousemove', (e) => {
    glitter.viewAngles(0,
      (e.clientX/window.innerWidth-0.5)*2*180,
      (e.clientY/window.innerHeight-0.5)*2*90);
  });

  // settings
  let settingsDiv = document.getElementById('glitter-settings');
  let toggleSettings = document.getElementById('toggle');
  toggleSettings.addEventListener('click', () => {
    settingsDiv.style.visibility = settingsDiv.style.visibility === 'hidden' ? '' : 'hidden';
  });
  settingsDiv.style.visibility = 'hidden';

  let color = document.getElementById('glitter-color');
  color.value = serializeColor(glitter.color);
  color.addEventListener('change', (e) => {
    glitter.color = color.value;
  });

  let iridescence = document.getElementById('glitter-iridescence');
  iridescence.addEventListener('change', (e) => {
    if (iridescence.checked) {
      glitter.color = 'iridescence';
      color.style.display = 'none';
    } else {
      glitter.color = color.value;
      color.style.display = '';
    }
  });
  iridescence.checked = false;

  let image = document.getElementById('image-upload');
  image.addEventListener('change', () => {
    if (image.files.length > 0) {
      const img = window.URL.createObjectURL(image.files[0]);
      canvas.style.backgroundImage = `url(${img})`;
    }
  });

  document.getElementById('fullscreen').addEventListener('click', () => {
    if (!document.fullscreenElement) {
      document.documentElement.requestFullscreen();
    } else {
      document.exitFullscreen();
    }
  });

  // drag
  canvas.addEventListener('dragover', (e) => {
    e.preventDefault();
    e.dataTransfer.dropEffect = 'move';
  });

  canvas.addEventListener('drop', (e) => {
    e.preventDefault();

    let img = e.dataTransfer.getData('text/uri-list');
    canvas.style.backgroundImage = `url(${img})`;
  });
}

init();
